CREATE TABLE store
(
    id           UUID        DEFAULT uuid_generate_v1() PRIMARY KEY NOT NULL,
    time_created TIMESTAMPTZ DEFAULT clock_timestamp()              NOT NULL,
    time_updated TIMESTAMPTZ DEFAULT clock_timestamp()              NOT NULL,
    name         VARCHAR(50)                                        NOT NULL,
    address      TEXT                                               NOT NULL,
    company_id   UUID REFERENCES company (id)                       NOT NULL
);
CREATE TRIGGER update_store_trg
    BEFORE UPDATE
    ON store
    FOR EACH ROW
EXECUTE PROCEDURE update_check_fn();
CREATE INDEX store__company_id_idx ON store (company_id);