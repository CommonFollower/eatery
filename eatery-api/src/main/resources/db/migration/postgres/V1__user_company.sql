CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE EXTENSION IF NOT EXISTS "pgcrypto";

CREATE FUNCTION update_check_fn()
    RETURNS TRIGGER AS
    $$
BEGIN
    IF new.id <> old.id THEN -- help ensure that the primary can't be updated once it is created
        RAISE EXCEPTION 'cannot update id once it has been created';
    END IF;

    new.time_updated := clock_timestamp();
RETURN new;
END;
$$
LANGUAGE plpgsql;

CREATE TABLE company
(
    id           UUID        DEFAULT uuid_generate_v1()   PRIMARY KEY NOT NULL,
    time_created TIMESTAMPTZ DEFAULT clock_timestamp()                NOT NULL,
    time_updated TIMESTAMPTZ DEFAULT clock_timestamp()                NOT NULL,
    name         VARCHAR(50) CHECK ( char_length(trim(name)) > 1 )    NOT NULL
);
CREATE TRIGGER update_company_trg
    BEFORE UPDATE
    ON company
    FOR EACH ROW
    EXECUTE PROCEDURE update_check_fn();

CREATE TABLE sec_user
(
    id           UUID        DEFAULT uuid_generate_v1() PRIMARY KEY    NOT NULL,
    time_created TIMESTAMPTZ DEFAULT clock_timestamp()                 NOT NULL,
    time_updated TIMESTAMPTZ DEFAULT clock_timestamp()                 NOT NULL,
    username     VARCHAR(100) CHECK ( char_length(trim(username)) > 1) NOT NULL,
    password     TEXT CHECK ( char_length(trim(password)) > 1 )        NOT NULL,
    enabled      BOOLEAN     DEFAULT TRUE                              NOT NULL,
    company_id   UUID REFERENCES company (id)                          NOT NULL,
    UNIQUE (username, company_id, enabled)
);
CREATE TRIGGER update_sec_user_trg
    BEFORE UPDATE
    ON sec_user
    FOR EACH ROW
    EXECUTE PROCEDURE update_check_fn();
CREATE INDEX sec_user__company_id_idx ON sec_user (company_id);

CREATE TABLE sec_role_type_domain
(
    id                INTEGER PRIMARY KEY NOT NULL,
    value             VARCHAR(10)         NOT NULL,
    description       VARCHAR(100)        NOT NULL,
    localization_code VARCHAR(100)        NOT NULL
);
INSERT INTO sec_role (id, value, description, localization_code)
VALUES (1, 'ADMIN', 'Eatery System Administrator', 'system.administrator'),
       (2, 'COMPANY_ADMIN', 'Company Administrator', 'company.administrator'),
       (3, 'MENU_MANAGER', 'Company Menu Manager', 'company.menu.manager');

CREATE TABLE sec_user_role
(
    sec_user_id UUID REFERENCES sec_user (id) NOT NULL,
    sec_role_id UUID REFERENCES sec_role (id) NOT NULL
);
