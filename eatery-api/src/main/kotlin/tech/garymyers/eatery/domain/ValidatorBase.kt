package tech.garymyers.eatery.domain

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import tech.garymyers.eatery.api.error.SystemValidationException
import tech.garymyers.eatery.api.error.ValidationError

abstract class ValidatorBase {
   companion object {
      val logger: Logger = LoggerFactory.getLogger(ValidatorBase::class.java)
   }

   @Throws(SystemValidationException::class)
   protected fun doValidation(validator: (MutableSet<ValidationError>) -> Unit) {
      val errors = mutableSetOf<ValidationError>()

      validator(errors)

      if (errors.isNotEmpty()) {
         logger.warn("Validation encountered errors {}", errors)

         throw SystemValidationException(errors)
      }
   }
}
