package tech.garymyers.eatery.domain

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonInclude.Include.ALWAYS
import kotlin.math.ceil

@JsonInclude(ALWAYS)
data class ContollerPage<I>(

   val elements: List<I> = emptyList(),

   val requested: ControllerPageRequest,

   val totalElements: Long,

   val totalPages: Long = ceil(totalElements.toDouble() / (requested.size()).toDouble()).toLong(),

   val first: Boolean = requested.page() == 1,

   val last: Boolean = requested.page().toLong() == totalPages
)
