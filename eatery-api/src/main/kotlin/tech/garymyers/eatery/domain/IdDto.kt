package tech.garymyers.eatery.domain

import io.micronaut.core.annotation.Introspected
import java.util.UUID
import javax.validation.constraints.NotNull

@Introspected
data class IdDto(

   @field:NotNull
   var id: UUID? = null
) {
   constructor(identifiable: Identifiable) :
      this(
         id = identifiable.myId()
      )
}
