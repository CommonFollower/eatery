package tech.garymyers.eatery.domain

import java.util.UUID

interface Identifiable {
   fun myId(): UUID?
}
