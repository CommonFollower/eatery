package tech.garymyers.eatery.extensions

import io.micronaut.data.repository.CrudRepository

fun <E, ID> CrudRepository<E, ID>.findByIdOrNull(id: ID): E? =
   this.findById(id!!).orNull()
