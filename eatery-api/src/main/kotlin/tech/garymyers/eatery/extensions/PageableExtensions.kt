package tech.garymyers.eatery.extensions

import io.micronaut.data.model.Pageable
import io.micronaut.data.model.Sort

fun Pageable?.defaultPage(): Pageable {
   return this ?: Pageable.from(0, 10, Sort.of(Sort.Order("id")))
}
