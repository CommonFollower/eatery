package tech.garymyers.eatery.extensions

import io.micronaut.data.model.Page
import tech.garymyers.eatery.domain.ContollerPage
import tech.garymyers.eatery.domain.ControllerPageRequest

fun <T, R> Page<T>.toControllerPage(requested: ControllerPageRequest, transformer: (t: T) -> R): ContollerPage<R> {
   val transformedElements = this.map { transformer(it) }

   return ContollerPage(
      elements = transformedElements.content,
      totalElements = this.totalSize,
      requested = requested.copyFillInDefaults()
   )
}
