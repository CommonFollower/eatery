package tech.garymyers.eatery.api.authentication.user

import io.micronaut.security.authentication.Authentication
import tech.garymyers.eatery.api.authentication.user.infrastructure.UserRepository
import tech.garymyers.eatery.api.error.NotFoundException
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserAuthenticationService @Inject constructor(
   private val userRepository: UserRepository
) {

   fun findUser(authentication: Authentication): User? {
      val userId = authentication.attributes["userId"]?.let { UUID.fromString(it as String) } ?: throw NotFoundException("Unable to find logged in user")
      val companyId = authentication.attributes["companyId"]?.let { UUID.fromString(it as String) } ?: throw NotFoundException("Unable to find logged in user")

      return userRepository.findByIdAndCompanyId(userId, companyId)
   }
}