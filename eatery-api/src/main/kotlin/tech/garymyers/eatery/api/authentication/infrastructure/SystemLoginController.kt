package tech.garymyers.eatery.api.authentication.infrastructure

import io.micronaut.http.HttpRequest
import io.micronaut.http.MediaType.APPLICATION_JSON
import io.micronaut.http.MutableHttpResponse
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Consumes
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Post
import io.micronaut.security.annotation.Secured
import io.micronaut.security.endpoints.LoginController
import io.micronaut.security.rules.SecurityRule.IS_ANONYMOUS
import io.micronaut.validation.Validated
import io.reactivex.Single
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import tech.garymyers.eatery.api.authentication.LoginCredentials
import javax.inject.Inject
import javax.validation.Valid

@Validated
@Secured(IS_ANONYMOUS)
@Controller("/api/login")
class SystemLoginController @Inject constructor(
   private val loginController: LoginController,
) {
   private val logger: Logger = LoggerFactory.getLogger(SystemLoginController::class.java)

   @Post
   @Consumes(APPLICATION_JSON)
   fun login(
      @Valid @Body
      loginCredentials: LoginCredentials,
      request: HttpRequest<*>
   ): Single<MutableHttpResponse<*>> {
      logger.info("Company login attempted with {}", loginCredentials)

      return loginController.login(loginCredentials, request)
   }
}