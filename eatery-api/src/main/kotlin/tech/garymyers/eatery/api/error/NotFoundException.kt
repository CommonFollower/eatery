package tech.garymyers.eatery.api.error

import java.util.UUID

class NotFoundException(
   message: String
) : Exception(message) {
   constructor(id: UUID) :
      this(message = id.toString())
}
