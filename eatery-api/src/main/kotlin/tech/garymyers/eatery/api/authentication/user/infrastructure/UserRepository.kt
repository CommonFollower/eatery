package tech.garymyers.eatery.api.authentication.user.infrastructure

import io.micronaut.data.annotation.Join
import io.micronaut.data.annotation.Query
import io.micronaut.data.jdbc.annotation.JdbcRepository
import io.micronaut.data.repository.PageableRepository
import io.reactivex.Maybe
import org.jdbi.v3.core.Jdbi
import tech.garymyers.eatery.api.authentication.user.AuthenticatedUser
import tech.garymyers.eatery.extensions.getOffsetDateTime
import tech.garymyers.eatery.extensions.getUuid
import java.util.UUID
import javax.transaction.Transactional

@JdbcRepository
abstract class UserRepository(
   private val jdbi: Jdbi,
) : PageableRepository<AuthenticatedUser, UUID> {

   @Transactional
   fun save(user: AuthenticatedUser): AuthenticatedUser {
      return jdbi.withHandle<AuthenticatedUser, Exception> { handle ->
         handle.createQuery(
            """
               INSERT INTO sec_user (username, password, company_id)
               VALUES (:username, crypt(:password, gen_salt('bf')), :company_id)
               RETURNING *
            """.trimIndent()
         )
            .bind("username", user.username)
            .bind("password", user.password)
            .bind("company_id", user.company.id)
            .map { rs, _ ->
               AuthenticatedUser(
                  rs.getUuid("id"),
                  rs.getOffsetDateTime("time_created"),
                  rs.getOffsetDateTime("time_updated"),
                  rs.getString("username"),
                  rs.getString("password"),
                  company = user.company
               )
            }
            .findOne().orElse(null)
      }
   }

   @Query("""
      SELECT
        su.*,
        c.id AS company_id,
        c.time_created AS company_time_created,
        c.time_updated AS company_time_updated,
        c.name AS company_name
      FROM sec_user su
           JOIN company c on su.company_id = c.id
      WHERE su.username = :username
            AND password = crypt(:password, password)
   """)
   @Join(value = "company")
   abstract fun findByCompanyIdAndUsernameAndPassword(
      companyId: UUID,
      username: String,
      password: String
   ): Maybe<AuthenticatedUser>

   @Join(value = "company")
   abstract fun findByIdAndCompanyId(id: UUID, companyId: UUID): AuthenticatedUser?
}
