package tech.garymyers.eatery.api.authentication.infrastructure

import io.micronaut.http.HttpRequest
import io.micronaut.security.authentication.AuthenticationFailed
import io.micronaut.security.authentication.AuthenticationFailureReason.CREDENTIALS_DO_NOT_MATCH
import io.micronaut.security.authentication.AuthenticationProvider
import io.micronaut.security.authentication.AuthenticationRequest
import io.micronaut.security.authentication.AuthenticationResponse
import io.micronaut.security.authentication.UserDetails
import io.reactivex.Flowable.just
import org.reactivestreams.Publisher
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import tech.garymyers.eatery.api.authentication.LoginCredentials
import tech.garymyers.eatery.api.authentication.user.infrastructure.UserRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserAuthenticationProvider @Inject constructor(
   private val userRepository: UserRepository
) : AuthenticationProvider {
   private val logger: Logger = LoggerFactory.getLogger(UserAuthenticationProvider::class.java)

   override fun authenticate(
      httpRequest: HttpRequest<*>?,
      authenticationRequest: AuthenticationRequest<*, *>?
   ): Publisher<AuthenticationResponse> {
      logger.debug("Attempting authentication for {}", authenticationRequest?.identity)

      val identity = (authenticationRequest?.identity as String?)
      val secret = authenticationRequest?.secret as String?
      val companyId = if (authenticationRequest is LoginCredentials) authenticationRequest.companyId else null

      return if (identity != null && secret != null && companyId != null) {
         userRepository.findByCompanyIdAndUsernameAndPassword(companyId, identity, secret)
            .flatMapPublisher {
               just(
                  UserDetails(
                     it.username,
                     mutableListOf(),
                     mapOf(
                        "companyId" to it.company.id.toString(),
                        "userId" to it.id.toString()
                     )
                  ) as AuthenticationResponse
               )
            }
            .defaultIfEmpty(AuthenticationFailed(CREDENTIALS_DO_NOT_MATCH))
      } else {
         just(AuthenticationFailed(CREDENTIALS_DO_NOT_MATCH))
      }
   }
}