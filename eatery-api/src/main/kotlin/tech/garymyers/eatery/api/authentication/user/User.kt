package tech.garymyers.eatery.api.authentication.user

import tech.garymyers.eatery.api.company.Company
import java.util.*

interface User {
   fun myId(): UUID
   fun myCompany(): Company
}
