package tech.garymyers.eatery.api.company.infrastructure

import io.micronaut.data.jdbc.annotation.JdbcRepository
import io.micronaut.data.repository.PageableRepository
import tech.garymyers.eatery.api.company.Company
import java.util.UUID

@JdbcRepository
interface CompanyRepository : PageableRepository<Company, UUID>
