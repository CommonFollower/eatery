package tech.garymyers.eatery.api.authentication

import io.micronaut.security.authentication.UsernamePasswordCredentials
import tech.garymyers.eatery.api.authentication.user.AuthenticatedUser
import java.util.*

class LoginCredentials(
   username: String? = null,
   password: String? = null,
   var companyId: UUID? = null
) : UsernamePasswordCredentials(
   username,
   password
) {
   constructor(authenticatedUser: AuthenticatedUser) :
      this(
         username = authenticatedUser.username,
         password = authenticatedUser.password,
         companyId = authenticatedUser.id,
      )
}