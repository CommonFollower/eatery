package tech.garymyers.eatery.api.company

import io.micronaut.data.model.Page
import io.micronaut.data.model.Pageable
import tech.garymyers.eatery.api.company.infrastructure.CompanyRepository
import tech.garymyers.eatery.extensions.findByIdOrNull
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CompanyService @Inject constructor(
   private val companyRepository: CompanyRepository,
) {
   fun findOne(id: UUID): Company? =
      companyRepository.findByIdOrNull(id)

   fun findAll(pageable: Pageable): Page<Company> =
      companyRepository.findAll(pageable)

   fun save(company: Company): Company =
      companyRepository.save(company)

   fun update(company: Company): Company =
      companyRepository.update(company)
}