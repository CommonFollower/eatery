package tech.garymyers.eatery.api.authentication.user

import io.micronaut.data.annotation.GeneratedValue
import io.micronaut.data.annotation.Id
import io.micronaut.data.annotation.MappedEntity
import io.micronaut.data.annotation.Relation
import io.micronaut.data.annotation.Relation.Kind.MANY_TO_ONE
import tech.garymyers.eatery.api.company.Company
import java.time.OffsetDateTime
import java.util.*

@MappedEntity("sec_user")
data class AuthenticatedUser(

   @field:Id
   @field:GeneratedValue
   val id: UUID? = null,

   @field:GeneratedValue
   var timeCreated: OffsetDateTime? = null,

   @field:GeneratedValue
   var timeUpdated: OffsetDateTime? = null,

   val username: String,

   val password: String,

   @field:Relation(value = MANY_TO_ONE)
   val company: Company,

   ) : User {
   override fun myId(): UUID = id!!
   override fun myCompany(): Company = company
}