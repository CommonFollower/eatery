package tech.garymyers.eatery.api.company

import io.micronaut.data.annotation.GeneratedValue
import io.micronaut.data.annotation.Id
import io.micronaut.data.annotation.MappedEntity
import tech.garymyers.eatery.domain.Identifiable
import java.time.OffsetDateTime
import java.util.*

@MappedEntity
data class Company(

   @field:Id
   @field:GeneratedValue
   var id: UUID? = null,

   @field:GeneratedValue
   var timeCreated: OffsetDateTime? = null,

   @field:GeneratedValue
   var timeUpdated: OffsetDateTime? = null,

   var name: String
) : Identifiable {
   constructor(name: String) :
      this(
         id = null,
         name = name
      )

   constructor(dto: CompanyDto, id: UUID? = null) :
      this(
         id = id,
         name = dto.name!!
      )

   override fun myId(): UUID? = id
}
