package tech.garymyers.eatery.api.error

class SystemValidationException(
   val errors: Set<ValidationError>
) : Exception()

data class ValidationError(
   val path: String?,
   val message: String,
) {
   constructor(message: String) :
      this(
         path = null,
         message = message,
      )
}
