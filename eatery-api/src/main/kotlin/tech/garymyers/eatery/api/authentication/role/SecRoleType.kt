package tech.garymyers.eatery.api.authentication.role

sealed class SecRoleType(
   val id: Int,
   val value: String,
   val description: String,
   val localizationCode: String,
)

class SecRoleTypeEntity(
   id: Int,
   value: String,
   description: String,
   localizationCode: String
) : SecRoleType(id, value, description, localizationCode)

object Admin: SecRoleType(1, "ADMIN", "Eatery System Administrator", "system.administrator")
object CompanyAdmin: SecRoleType(1, "COMPANY_ADMIN", "Company Administrator", "company.administrator")
object MenuManager: SecRoleType(1, "MENU_MANAGER", "Company Menu Manager", "company.menu.manager")

typealias ADMIN = Admin
typealias COMPANY_ADMIN = CompanyAdmin
typealias MENU_MANAGER = MenuManager
