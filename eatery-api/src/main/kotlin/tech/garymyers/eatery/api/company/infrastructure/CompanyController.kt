package tech.garymyers.eatery.api.company.infrastructure

import io.micronaut.http.HttpStatus
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.PathVariable
import io.micronaut.http.annotation.Post
import io.micronaut.http.annotation.Put
import io.micronaut.http.annotation.QueryValue
import io.micronaut.http.annotation.Status
import io.micronaut.security.annotation.Secured
import io.micronaut.security.rules.SecurityRule
import io.micronaut.security.rules.SecurityRule.IS_ANONYMOUS
import io.micronaut.security.rules.SecurityRule.IS_AUTHENTICATED
import io.micronaut.validation.Validated
import tech.garymyers.eatery.api.company.Company
import tech.garymyers.eatery.api.company.CompanyDto
import tech.garymyers.eatery.api.company.CompanyService
import tech.garymyers.eatery.domain.ContollerPage
import tech.garymyers.eatery.domain.StandardControllerPageRequest
import tech.garymyers.eatery.extensions.toControllerPage
import java.util.*
import javax.inject.Inject
import javax.validation.Valid

@Validated
@Secured(IS_AUTHENTICATED)
@Controller("/api/company")
class CompanyController @Inject constructor(
   private val companyService: CompanyService,
) {
   @Get("/{id}", produces = [MediaType.APPLICATION_JSON])
   fun readOne(
      @PathVariable("id") id: UUID
   ): CompanyDto? =
      companyService
         .findOne(id)
         ?.let { CompanyDto(it) }

   @Secured(IS_ANONYMOUS)
   @Get(value = "{?pageRequest*}", produces = [MediaType.APPLICATION_JSON])
   fun readAll(
      @QueryValue("pageRequest") pageRequest: StandardControllerPageRequest?,
   ): ContollerPage<CompanyDto> {
      val requested = pageRequest ?: StandardControllerPageRequest()

      return companyService
         .findAll(requested.toPageable())
         .toControllerPage(requested) { CompanyDto(it) }
   }

   @Secured(IS_AUTHENTICATED, "ROLE_ADMIN")
   @Status(HttpStatus.CREATED)
   @Post(processes = [MediaType.APPLICATION_JSON])
   fun create(
      @Valid @Body
      companyDto: CompanyDto
   ): CompanyDto =
      CompanyDto(companyService.save(Company(companyDto)))

   @Secured(IS_AUTHENTICATED, "ROLE_ADMIN")
   @Put("/{id}", processes = [MediaType.APPLICATION_JSON])
   fun update(
      @PathVariable("id") id: UUID,
      @Valid @Body
      companyDto: CompanyDto
   ): CompanyDto =
      CompanyDto(companyService.update(Company(companyDto, id)))
}