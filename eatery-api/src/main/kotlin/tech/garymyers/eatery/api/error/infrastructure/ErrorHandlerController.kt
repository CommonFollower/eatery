package tech.garymyers.eatery.api.error.infrastructure

import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Error
import org.apache.commons.lang3.StringUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import tech.garymyers.eatery.api.error.ErrorDTO
import tech.garymyers.eatery.api.error.NotFoundException
import javax.validation.ConstraintViolationException
import javax.validation.Path

@Controller
class ErrorHandlerController {
   private val logger: Logger = LoggerFactory.getLogger(ErrorHandlerController::class.java)

   @Error(global = true, exception = NotFoundException::class)
   fun notFoundExceptionHandler(notFoundException: NotFoundException): HttpResponse<ErrorDTO> {
      logger.warn("Not found", notFoundException)

      return HttpResponse.notFound(ErrorDTO(notFoundException.message!!))
   }

   @Error(global = true, exception = ConstraintViolationException::class)
   fun constraintViolationException(constraintViolationException: ConstraintViolationException): HttpResponse<List<ErrorDTO>> {
      val errors = constraintViolationException.constraintViolations.map {
         val field = buildPropertyPath(rootPath = it.propertyPath)
         val value = it.invalidValue ?: StringUtils.EMPTY

         ErrorDTO(message = "$field ${it.message}", path = field)
      }

      return HttpResponse.badRequest(errors)
   }

   private fun buildPropertyPath(rootPath: Path): String =
      rootPath.asSequence().joinToString(".")
}
