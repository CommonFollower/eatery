package tech.garymyers.eatery.domain

import io.micronaut.core.type.Argument
import io.micronaut.http.client.BlockingHttpClient
import io.micronaut.http.client.RxHttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.http.client.exceptions.HttpClientResponseException
import io.micronaut.security.token.jwt.render.BearerAccessRefreshToken
import tech.garymyers.eatery.api.authentication.LoginCredentials
import tech.garymyers.eatery.api.authentication.user.AuthenticatedUser
import tech.garymyers.eatery.api.authentication.user.UserTestDataLoaderService
import tech.garymyers.eatery.api.company.Company
import tech.garymyers.eatery.api.company.CompanyTestDataLoaderService

import javax.inject.Inject

import static io.micronaut.http.HttpRequest.*

class ControllerSpecificationBase extends ServiceSpecificationBase {
   @Inject CompanyTestDataLoaderService companyTestDataLoaderService
   @Client("/api") @Inject RxHttpClient httpClient
   @Inject UserTestDataLoaderService userTestDataLoaderService

   BlockingHttpClient client
   Company company
   AuthenticatedUser user
   String accessToken

   void setup() {
      this.client = httpClient.toBlocking()
      this.company = companyTestDataLoaderService.single()
      this.user = userTestDataLoaderService.single(company)
      this.accessToken = client.exchange(POST("/login", new LoginCredentials(user)), BearerAccessRefreshToken).body().accessToken
   }

   Object get(String path, String accessToken = accessToken) throws HttpClientResponseException {
      return client.exchange(
         GET("/${path}").bearerAuth(accessToken),
         Argument.of(String),
         Argument.of(String)
      ).bodyAsJson()
   }

   Object post(String path, Object body, String accessToken = accessToken) throws HttpClientResponseException {
      return client.exchange(
         POST("/${path}", body).bearerAuth(accessToken),
         Argument.of(String),
         Argument.of(String)
      ).bodyAsJson()
   }

   Object put(String path, Object body, String accessToken = accessToken) throws HttpClientResponseException {
      return client.exchange(
         PUT("/${path}", body).bearerAuth(accessToken),
         Argument.of(String),
         Argument.of(String)
      ).bodyAsJson()
   }

   Object delete(String path, String accessToken = accessToken) throws HttpClientResponseException {
      return client.exchange(
         DELETE("/${path}").bearerAuth(accessToken),
         Argument.of(String),
         Argument.of(String)
      ).bodyAsJson()
   }
}
