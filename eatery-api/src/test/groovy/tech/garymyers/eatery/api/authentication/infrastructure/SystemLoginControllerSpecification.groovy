package tech.garymyers.eatery.api.authentication.infrastructure

import io.micronaut.core.type.Argument
import io.micronaut.http.HttpRequest
import io.micronaut.http.client.RxHttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.test.extensions.spock.annotation.MicronautTest
import tech.garymyers.eatery.api.authentication.LoginCredentials
import tech.garymyers.eatery.api.authentication.user.UserTestDataLoaderService
import tech.garymyers.eatery.api.company.CompanyTestDataLoaderService
import tech.garymyers.eatery.domain.ServiceSpecificationBase

import javax.inject.Inject

@MicronautTest(transactional = false)
class SystemLoginControllerSpecification extends ServiceSpecificationBase {
   @Inject @Client("/api") RxHttpClient httpClient
   @Inject CompanyTestDataLoaderService companyTestDataLoaderService
   @Inject UserTestDataLoaderService userTestDataLoaderService

   void 'login successful' () {
      given:
      final company = companyTestDataLoaderService.single()
      final user = userTestDataLoaderService.single(company)

      when:
      def authResponse = httpClient.toBlocking()
         .exchange(
            HttpRequest.POST("/login", new LoginCredentials(user.username, user.password, company.id)),
            Argument.of(String),
            Argument.of(String),
         ).bodyAsJson()

      then:
      notThrown(Exception)
      authResponse.access_token != null
   }
}
