package tech.garymyers.eatery.api.authentication.user

import com.github.javafaker.Faker
import io.micronaut.context.annotation.Requires
import tech.garymyers.eatery.api.authentication.user.infrastructure.UserRepository
import tech.garymyers.eatery.api.company.Company

import javax.inject.Singleton
import java.util.stream.IntStream
import java.util.stream.Stream

class UserTestDataLoader {
   static Stream<AuthenticatedUser> load(int numberIn = 1, Company company) {
      final number = numberIn > 0 ? numberIn : 1
      final faker = new Faker()
      final name = faker.name()
      final lorem = faker.lorem()

      return IntStream.range(0, number).mapToObj {
         new AuthenticatedUser(
            null,
            null,
            null,
            name.username(),
            lorem.characters(4, 15, true, true),
            company
         )
      }
   }
}

@Singleton
@Requires(env = ["test"])
class UserTestDataLoaderService {
   private final UserRepository userRepository

   UserTestDataLoaderService(UserRepository userRepository) {
      this.userRepository = userRepository
   }

   Stream<AuthenticatedUser> load(int numberIn = 1, Company company) {
      return UserTestDataLoader.load(numberIn, company)
         .map {
            final password = it.password
            final savedUser = userRepository.save(it)

            return new AuthenticatedUser(
               savedUser.id,
               savedUser.timeCreated,
               savedUser.timeUpdated,
               savedUser.username,
               password,
               company
            )
         }
   }

   AuthenticatedUser single(Company company) {
      return load(1, company).findFirst().orElseThrow { new Exception("Unable to create single Company") }
   }
}
