package tech.garymyers.eatery.api.company

import com.github.javafaker.Faker
import io.micronaut.context.annotation.Requires
import tech.garymyers.eatery.api.company.infrastructure.CompanyRepository

import javax.inject.Singleton
import java.util.stream.IntStream
import java.util.stream.Stream

class CompanyTestDataLoader {
   static Stream<Company> load(int numberIn = 1) {
      final number = numberIn > 0 ? numberIn : 1
      final faker = new Faker()
      final company = faker.company()

      IntStream.range(0, number).mapToObj {
         new Company(company.name())
      }
   }

   static Company single() {
      load().findFirst().orElseThrow { new Exception("Unable to create Company")}
   }
}

@Singleton
@Requires(env = ["test"])
class CompanyTestDataLoaderService {
   private final CompanyRepository companyRepository

   CompanyTestDataLoaderService(CompanyRepository companyRepository) {
      this.companyRepository = companyRepository
   }

   Stream<Company> load(int numberIn = 1) {
      return CompanyTestDataLoader.load(numberIn)
         .map(companyRepository::save) as Stream<Company>
   }

   Company single() {
      return load().findFirst().orElseThrow { new Exception("Unable to create Company") }
   }
}
