package tech.garymyers.eatery;

import groovy.sql.Sql
import io.micronaut.context.annotation.Bean
import io.micronaut.context.annotation.Factory
import javax.sql.DataSource

@Factory
class TestingBeanFactory {

    @Bean
    Sql groovySql(DataSource dataSource) {
        return new Sql(dataSource)
    }
}