package tech.garymyers.eatery

import io.micronaut.test.extensions.spock.annotation.MicronautTest
import spock.lang.Requires
import spock.lang.Specification
import tech.garymyers.eatery.api.authentication.user.User
import tech.garymyers.eatery.api.authentication.user.UserTestDataLoaderService
import tech.garymyers.eatery.api.company.Company
import tech.garymyers.eatery.api.company.CompanyTestDataLoaderService
import tech.garymyers.eatery.domain.TruncateDatabaseService

import javax.inject.Inject

@Requires({ System.getenv("SEED_DATA")?.equals("true") })
@MicronautTest(transactional = false, environments = "load")
class SeedTestData extends Specification {
   @Inject CompanyTestDataLoaderService companyTestDataLoaderService
   @Inject TruncateDatabaseService truncateDatabaseService
   @Inject UserTestDataLoaderService userTestDataLoaderService

   void setup() {
      truncateDatabaseService.truncate()
   }

   void "load data" () {
      given:
      final List<Company> companies = companyTestDataLoaderService.load(2).toList()
      final List<User> users = companies.stream().flatMap {userTestDataLoaderService.load(2, it) }.peek { println(it) }.toList()

      expect:
      companies.size() == 2
      users.size() == 4

      cleanup:
      users.each {
         println it.username + " - " + it.password
      }
   }
}
