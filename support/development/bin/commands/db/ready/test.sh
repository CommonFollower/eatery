#!/usr/bin/env bash
## description: starts theeaterytestdb database makes it available on localhost:7432 and returns once they are up and running

cd ../docker

daemon="-d"

if [ "$1" == "--nodaemon" ]; then
  daemon=""
fi

if [ -z `docker-compose ps -q eaterytestdb` ] || [ -z `docker ps -q --no-trunc | grep $(docker-compose ps -q eaterytestdb)` ]; then
  docker-compose build --force-rm --quiet eaterybasedb

  if [ "$daemon" == "-d" ]; then
    docker-compose build --force-rm --quiet eaterytestdbready && docker-compose run --no-deps --rm eaterytestdbready
  fi

  exit $?
else
  echo "eaterytestdb is already running checking if it is accepting connections"
  docker-compose build --force-rm --quiet eaterytestdbready && docker-compose run --rm eaterytestdbready
  echo "can be accessed at $(docker-compose port eaterytestdb 5432)"
  exit 1
fi
