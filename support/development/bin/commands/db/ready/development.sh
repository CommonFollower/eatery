#!/usr/bin/env bash
## description: starts the eaterydb localhost:6432 and returns once it is up and running

cd ../docker

daemon="-d"

if [ "$1" == "--nodaemon" ]; then
  daemon=""
fi

if [ -z `docker-compose ps -q eaterydb` ] || [ -z `docker ps -q --no-trunc | grep $(docker-compose ps -q eaterydb)` ]; then
  docker-compose build --force-rm --quiet eaterybasedb

  if [ "$daemon" == "-d" ]; then
    docker-compose build --force-rm --quiet eaterydbready && docker-compose run --no-deps --rm eaterydbready
  fi

  exit $?
else
  echo "eaterydb are already running checking if it is accepting connections"
  docker-compose build eaterydbready && docker-compose run --rm eaterydbready
  echo "can be accessed at $(docker-compose port eaterydb 5432)"
  exit 1
fi
