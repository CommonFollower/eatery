#!/usr/bin/env bash
## description: connect a psql prompt to the Postgres hosted eaterydb database

cd ../docker

if [ -z `docker-compose ps -q eaterydb` ] || [ -z `docker ps -q --no-trunc | grep $(docker-compose ps -q eaterydb)` ]; then
  echo "eaterydb is not running! Not connecting psql."
  exit 1
else
  echo "Connecting interactive psql to eaterydb"
  docker-compose build --force-rm --quiet eaterybasedb
  docker-compose run --rm eaterydbpsql
  exit $?
fi
