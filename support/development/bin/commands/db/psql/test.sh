#!/usr/bin/env bash
## description: connect a psql prompt to the Postgres hosted eaterytestdb database

cd ../docker

if [ -z `docker-compose ps -q eaterytestdb` ] || [ -z `docker ps -q --no-trunc | grep $(docker-compose ps -q eaterytestdb)` ]; then
  echo "eaterytestdb is not running! Not connecting psql."
  exit 1
else
  echo "Connecting interactive psql to eaterytestdb"
  docker-compose build --force-rm --quiet eaterybasedb
  docker-compose run --rm eaterytestdbpsql
  exit $?
fi
