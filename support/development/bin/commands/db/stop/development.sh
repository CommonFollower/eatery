#!/usr/bin/env bash
## description: stops the eaterydb service

cd ../docker

if [ -z `docker-compose ps -q eaterydb` ] || [ -z `docker ps -q --no-trunc | grep $(docker-compose ps -q eaterydb)` ]; then
  echo "eaterydb is not running"
  exit 1
else
  echo "stopping eaterydb"
  docker-compose stop eaterydb && docker-compose rm -f eaterydb
  exit $?
fi
