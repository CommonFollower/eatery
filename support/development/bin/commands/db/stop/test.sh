#!/usr/bin/env bash
## description: stops the eaterydb service

cd ../docker

if [ -z `docker-compose ps -q eaterytestdb` ] || [ -z `docker ps -q --no-trunc | grep $(docker-compose ps -q eaterytestdb)` ]; then
  echo "eaterytestdb is not running"
  exit 1
else
  echo "stopping eaterytestdb"
  docker-compose stop eaterytestdb && docker-compose rm -f eaterytestdb
  exit $?
fi
