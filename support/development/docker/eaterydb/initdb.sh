#!/usr/bin/env sh

dropdb --if-exists eaterydb
createdb eaterydb
psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "eaterydb" <<-EOSQL
   CREATE USER eateryuser WITH PASSWORD 'password';
   ALTER ROLE eateryuser SUPERUSER;
EOSQL
