FROM eaterybasedb

COPY initdb.sh /docker-entrypoint-initdb.d/initdb.sh
COPY initdb.sql /docker-entrypoint-initdb.d/initdb.sql
RUN dos2unix /docker-entrypoint-initdb.d/initdb.sh
RUN dos2unix /docker-entrypoint-initdb.d/initdb.sql