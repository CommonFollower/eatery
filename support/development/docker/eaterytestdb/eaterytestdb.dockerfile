FROM eaterybasedb

COPY eatery-inittestdb.sh /docker-entrypoint-initdb.d/initdb.sh
COPY eatery-inittestdb.sql /docker-entrypoint-initdb.d/initdb.sql
